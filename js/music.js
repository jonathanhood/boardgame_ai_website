var app = angular.module('musicApp', []);

app.factory('audio', ['$document', function($document) {
    var audio = $document[0].createElement('audio');
    audio.src = '/media/danceTunes.mp3';
    return audio
}]);

app.factory('musicPlayer', ['$rootScope', 'audio', function($rootScope, audio) {
    var musicPlayer = {
        playing: false,

        play: function() {
            audio.play();
            musicPlayer.playing = true;
        },

        stop: function() {
            audio.pause();
            musicPlayer.playing = false;
        }
    }
    audio.addEventListener('ended', function() {
        $rootScope.$apply(musicPlayer.stop());
    });
    return musicPlayer;
}]);

app.controller('MusicController', ['$scope', 'musicPlayer', function($scope, musicPlayer) {
    $scope.musicPlayer = musicPlayer;
}]);